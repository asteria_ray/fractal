#ifndef HEAD_H_INCLUDED
#define HEAD_H_INCLUDED

void carpet(double A, double B, double C, double D, int rec);
void triangle(double x1, double y1, double x2, double y2, double x3, double y3, int rec);
void curve(double x1, double y1, double x2, double y2, int rec);

void show_carpet(double X1, double Y1, double X2, double Y2, int rec);
void show_triangle(double x1, double y1, double x2, double y2, double x3, double y3, int rec);
void show_snowflake(double x1, double y1, double x2, double y2, int rec);

#endif // HEAD_H_INCLUDED
