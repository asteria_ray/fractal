#include <stdio.h>
#include <graphics.h>
#include "head.h"

void show_carpet(double X1, double Y1,
                 double X2, double Y2,
                 int rec)
{
    bar(0,0,640,480);
    if (rec)
    {
        setfillstyle(1, 1);
        bar(X1, Y1, X2, Y2);
        setfillstyle(1,15);
        carpet(X1, Y1, X2, Y2, rec);
    }
}
