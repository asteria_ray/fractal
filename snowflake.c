#include <stdio.h>
#include <graphics.h>
#include <math.h>
#include <locale.h>
#include "head.h"

void curve(double x1, double y1, double x2, double y2, int rec)
{
    if (rec == 0)
    {
        line(floor(x1 + 0.5), floor(y1 + 0.5), floor(x2 + 0.5), floor(y2 + 0.5));

        return;
    }

    double incrX = x2 - x1;
    double incrY = y2 - y1;

    double x3 = x1 + incrX/3;
    double y3 = y1 + incrY/3;

    double x4 = x2 - incrX/3;
    double y4 = y2 - incrY/3;

    double x5 = x3 + ((x4 - x3) + (y4 - y3)*sqrt(3))*0.5;
    double y5 = y3 + ((y4 - y3) - (x4 - x3)*sqrt(3))*0.5;

    curve(x1, y1, x3, y3, rec - 1);
    curve(x3, y3, x5, y5, rec - 1);
    curve(x5, y5, x4, y4, rec - 1);
    curve(x4, y4, x2, y2, rec - 1);
}
