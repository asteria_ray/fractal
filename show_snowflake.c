#include <stdio.h>
#include <graphics.h>
#include <math.h>
#include <locale.h>
#include "head.h"

void show_snowflake(double x1, double y1, double x2, double y2, int rec)
{
    bar(0,0,640,480);
    if (rec)
    {
        setcolor(1);
        setlinestyle(0, 0, 1);
        curve(x1, y1, x2, y2, rec);
    }
}
