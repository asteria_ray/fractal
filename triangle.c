#include <stdio.h>
#include <math.h>
#include <graphics.h>
#include "head.h"

void triangle(double x1, double y1,
              double x2, double y2,
              double x3, double y3,
              int rec)
{
    if (rec)
    {
        int points[] = {floor((x1 + x2)/2 + 0.5), floor((y1 + y2)/2 + 0.5),     /* �������� ����� */
                        floor((x2 + x3)/2 + 0.5), floor((y2 + y3)/2 + 0.5),
                        floor((x1 + x3)/2 + 0.5), floor((y1 + y3)/2 + 0.5)};
        fillpoly(3, points);

        triangle( x1,          y1,                /* �����   */
                 (x1 + x2)/2, (y1 + y2)/2,
                 (x1 + x3)/2, (y1 + y3)/2,
                 rec - 1);

        triangle((x1 + x2)/2, (y1 + y2)/2,        /* ������� */
                  x2,          y2,
                 (x2 + x3)/2, (y2 + y3)/2,
                  rec - 1);

        triangle((x1 + x3)/2, (y1 + y3)/2,        /* ������  */
                 (x2 + x3)/2, (y2 + y3)/2,
                  x3,          y3,
                  rec - 1);
    }
}
