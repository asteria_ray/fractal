#include <stdio.h>
#include <math.h>
#include <graphics.h>
#include "head.h"

void show_triangle(double x1, double y1,
                   double x2, double y2,
                   double x3, double y3,
                   int rec)
{
    bar(0,0,640,480);
    if (rec)
    {
        setfillstyle(1, 1);
        int first_points[] = {x1, y1, x2, y2, x3, y3};
        fillpoly(3, first_points);
        setfillstyle(1,15);
        triangle(x1, y1, x2, y2, x3, y3, rec);
    }
}
