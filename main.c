#include <stdio.h>
#include <graphics.h>
#include <locale.h>
#include "head.h"

int main()
{
    setlocale(LC_CTYPE, "Russian");

    int fract_type;
    printf("������� ����� ��������:     \n"
           "1 - ���� �����������       \n"
           "2 - ����������� ����������� \n"
           "3 - ������ ����             \n");
    scanf ("%i", &fract_type);

    int rec;
    int gd = DETECT, gm = DETECT;

    switch (fract_type)
    {
        case 1:
            printf("������� ������� ��������:\n");
            scanf("%d", &rec);

            initgraph(&gd, &gm, "");
            show_carpet(90, 10, 550, 470, rec);

            break;

        case 2:
            printf("������� ������� ��������:\n");
            scanf("%d", &rec);

            initgraph(&gd, &gm, "");
            show_triangle(10, 470, 320, 10, 630, 470, rec);

            break;

        case 3:
            printf("������� ������� ��������:\n");
            scanf("%d", &rec);

            initgraph(&gd, &gm, "");
            show_snowflake(10, 330, 630, 330, rec);

            break;

        default:
            printf ("�������� ����\n");
            return 1;
    }

    while (1)
    {
        int c = readkey();

        if(c == 72)
        {
            clearviewport();
            switch (fract_type)
            {
                case 1:
                    show_carpet(90, 10, 550, 470, ++rec);
                    break;
                case 2:
                    show_triangle(10, 470, 320, 10, 630, 470, ++rec);
                    break;
                case 3:
                    show_snowflake(10, 330, 630, 330, ++rec);
                    break;
            }
        }

        if(c == 80)
        {
            clearviewport();
            if (!rec)
            {
                ++rec;
            }
            switch (fract_type)
            {
                case 1:
                    show_carpet(90, 10, 550, 470, --rec);
                    break;
                case 2:
                    show_triangle(10, 470, 320, 10, 630, 470, --rec);
                    break;
                case 3:
                    show_snowflake(10, 330, 630, 330, --rec);
                    break;
            }
        }
    }

    return 0;
}
