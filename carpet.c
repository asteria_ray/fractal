#include <stdio.h>
#include <graphics.h>
#include "head.h"

void carpet(double X1, double Y1,
            double X2, double Y2,
            int rec)
{
    double X1_new, Y1_new, X2_new, Y2_new;

    if (rec)
    {
        X1_new = 2*X1/3 + X2/3;                                 /* ������ ������� ������� �� ��� ����� */
        X2_new = X1/3 + 2*X2/3;
        Y1_new = 2*Y1/3 + Y2/3;
        Y2_new = Y1/3 + 2*Y2/3;

        bar(X1_new, Y1_new, X2_new, Y2_new);                    /* �������� ������� � ������ ��������� */

        carpet(X1,     Y1,     X1_new, Y1_new, rec - 1);        /* ������� �����                       */
        carpet(X1_new, Y1,     X2_new, Y1_new, rec - 1);        /* �������                             */
        carpet(X2_new, Y1,     X2,     Y1_new, rec - 1);        /* ������� ������                      */
        carpet(X1,     Y1_new, X1_new, Y2_new, rec - 1);        /* �����                               */
        carpet(X2_new, Y1_new, X2,     Y2_new, rec - 1);        /* ������                              */
        carpet(X1,     Y2_new, X1_new, Y2,     rec - 1);        /* ������ �����                        */
        carpet(X1_new, Y2_new, X2_new, Y2,     rec - 1);        /* ������                              */
        carpet(X2_new, Y2_new, X2,     Y2,     rec - 1);        /* ������ ������                       */
    }
}
